package com.vmturbo.integration.camel;

import org.apache.camel.main.Main;

/**
 * A Camel Application
 */
public class MainApp {

    /**
     * A main() so we can easily run these routing rules in our IDE
     */
    public static void main(String... args) throws Exception {
        Main main = new Main();
        main.enableHangupSupport();
        main.addRouteBuilder(new Poller());
        main.addRouteBuilder(new ActionProcessor());
        main.addRouteBuilder(new ServiceNowProcessor());
        main.addRouteBuilder(new ServiceNowMonitor());
        main.run(args);
    }

}