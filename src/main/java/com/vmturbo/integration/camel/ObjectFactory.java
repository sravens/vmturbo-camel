//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.4 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2014.10.06 at 11:58:45 PM BST 
//


package com.vmturbo.integration.camel;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.vmturbo.integration.jaxb package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.vmturbo.integration.jaxb
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ActionItems }
     * 
     */
    public ActionItems createActionItems() {
        return new ActionItems();
    }

    /**
     * Create an instance of {@link ActionItems.ActionItem }
     * 
     */
    public ActionItems.ActionItem createActionItemsActionItem() {
        return new ActionItems.ActionItem();
    }

    /**
     * Create an instance of {@link ActionItems.ActionItem.Notification }
     * 
     */
    public ActionItems.ActionItem.Notification createActionItemsActionItemNotification() {
        return new ActionItems.ActionItem.Notification();
    }

}
