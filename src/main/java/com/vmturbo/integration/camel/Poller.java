package com.vmturbo.integration.camel;

import org.apache.camel.LoggingLevel;
import org.apache.camel.builder.RouteBuilder;

/**
 * A Camel Java DSL Router
 */
public class Poller extends RouteBuilder {

	/**
	 * Let's configure the Camel routing rules using Java code...
	 */
	public void configure() {

        // Trigger the actions using a Quartz timer
    	from("quartz2://vmturbo/actionpoller?fireNow=true&trigger.repeatInterval=60000")
    	.multicast()
    	.to("direct:GetVMTActions")
    	.to("direct:WatchSNOWCRApprovals")
    	.end();
    	
    	
    }
}
