package com.vmturbo.integration.camel;

import org.apache.camel.Exchange;
import org.apache.camel.LoggingLevel;
import org.apache.camel.Processor;
import org.apache.camel.builder.RouteBuilder;

import com.vmturbo.integration.camel.ActionItems.ActionItem;

/**
 * A Camel Java DSL Router
 */
public class ServiceNowProcessor extends RouteBuilder {

	/**
	 * Let's configure the Camel routing rules using Java code...
	 */
	public void configure() {

        // Trigger the actions using a Quartz timer
    	from("direct:RaiseSNOWCR")
//    	.choice()
    	// TODO - Resolve CNF error
//    	.when(simple("${body.type} is 'com.vmturbo.integration.camel.ActionItems.ActionItem'"))
			.log(LoggingLevel.INFO, "ServiceNow processing message")
			.setHeader("Accept", constant("application/json"))
			.setHeader("Content-Type", constant("application/json"))
//			.setHeader("Authorization", constant("Basic Vk1UcmVzdHVzZXI6UGFzc3dvcmQxMjM="))
			.setHeader(Exchange.HTTP_URI, constant("https://sandbox.service-now.com/api/now/table/change_request"))
			.setHeader("CamelHttpMethod", constant("POST"))
//			.setHeader("Cookie", constant("BIGipServerpool_sandbox=808634890.33598.0000; _ga=GA1.2.659009183.1412777426; JSESSIONID=AF990505CFA4D9C83BA826FD108F42AD; glide_user_route=glide.a95da93e9baa2354958aa3c67bd1a027"))
			.setHeader("Cookie", constant("glide_user_route=glide.6dd4e8af0b4592f77d4e8c176be3a646; BIGipServerpool_sandbox=439536138.33854.0000; JSESSIONID=D9812C70BE65DF1E9294F9A7578DAC2C; _gat=1; _gat_rollup=1; _ga=GA1.2.659009183.1412777426"))
			.process(new Processor() {
				@Override
				public void process(Exchange exchange) throws Exception {
					ActionItem actionItem = exchange.getIn().getBody(ActionItem.class);
					// TODO - Lookup / Create the CMDB CI from the target
					String body = "{ " + "\"description\":\"" + actionItem.getExplanation().replace("\"", "\\\"") + "\","
							+ "\"short_description\": \"" + actionItem.getDisplayName().replace("\"", "\\\"") + "\","
							+ "\"approval\": \"requested\","
							+ "\"user_input\": \"VMTuuid:" + actionItem.getUuid().toString() + "\""
//							+ "\"cmdb_ci\": { \"link\": \"https://sandbox.service-now.com/api/now/table/cmdb_ci/f8e0dd0795e32100b8cc018a870c6984\", \"value\": \"f8e0dd0795e32100b8cc018a870c6984\"    }\""
							+ "}";
					// TODO - Remove
					body = body.replaceAll("vmturbo.com", "domain.local");
					exchange.getIn().setBody(body);
				}
			})
//			.setBody()
//			.simple("{ \"description\":\"${body?.getExplanation},"
//					+ "\"short_description\": \"${body?.getDisplayName}\","
//					+ "\"approval\": \"requested\""
//					+ "}")
			.to("log:com.vmturbo.integration.camel?level=DEBUG&multiline=true&showAll=true")
			.inOut("http4://servicenow?authMethod=Basic&authenticationPreemptive=true&authUsername=VMTrestuser&authPassword=Password123")
			.convertBodyTo(String.class)
			.to("log:com.vmturbo.integration.camel?level=DEBUG&multiline=true&showAll=true")
			
    	.end();
    }

}
