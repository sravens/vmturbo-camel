package com.vmturbo.integration.camel;

import org.apache.camel.LoggingLevel;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.model.dataformat.JaxbDataFormat;
import org.apache.camel.processor.idempotent.MemoryIdempotentRepository;

/**
 * A Camel Java DSL Router
 */
public class ActionProcessor extends RouteBuilder {

	/**
	 * Let's configure the Camel routing rules using Java code...
	 */
	public void configure() {
		JaxbDataFormat jaxbDataFormat = new JaxbDataFormat();
		jaxbDataFormat.setContextPath(ActionItems.class.getPackage().getName());
		// jaxbDataFormat.setSchemaLocation("ActionItems.xsd");

		// Get Actions and Process them
		from("direct:GetVMTActions")
			.setHeader("CamelHttpMethod", constant("GET"))
			.inOut("http4://10.33.9.150/vmturbo/api/markets/Market/groups/Test/actionitems?authUsername=administrator&authPassword=administrator")
			.to("log:com.vmturbo.integration.camel?level=DEBUG&multiline=true&showAll=true")
			// TODO - Add error handler for unexpected response codes
			.filter(header("CamelHttpResponseCode").isEqualTo("200"))
				.unmarshal(jaxbDataFormat)
				// Split out a List of ActionItems
				.split(simple("${body?.getActionItem}"))
				// Remove duplicates using an Idempotent repo keyed off the uuid
				// TODO - Change to file store to persist across context restarts
				.idempotentConsumer(simple("${body?.getUuid}"),
						MemoryIdempotentRepository.memoryIdempotentRepository(200))
				// Choose what to do with an ActionItem
				.choice()
				// Pending Accept
				.when(simple("${body?.getState} == 'Pending Accept'"))
					.log(LoggingLevel.INFO, "PROCESSING ${body?.getState}")
					.to("direct:ProcessVMTAction")
				// Other state
				.otherwise()
					.log(LoggingLevel.INFO, "IGNORED ${body?.getState}")
			.end();
		
		
		from("direct:ProcessVMTAction")
			.multicast()
				.to("direct:RaiseSNOWCR")
			.end()
			.log(LoggingLevel.INFO, "DONE PROCESSING")
			.end();
	}
}
