package com.vmturbo.integration.camel;

import java.util.Map;

import org.apache.camel.Exchange;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.model.dataformat.JsonLibrary;

/**
 * A Camel Java DSL Router
 */
public class ServiceNowMonitor extends RouteBuilder {

	/**
	 * Let's configure the Camel routing rules using Java code...
	 */
	public void configure() {

        // Trigger the actions using a Quartz timer
    	from("direct:WatchSNOWCRApprovals")
		.setHeader("Accept", constant("application/json"))
		.setHeader("Content-Type", constant("application/json"))
		.setHeader(Exchange.HTTP_URI, constant("https://sandbox.service-now.com/api/now/table/change_request/?sysparm_query=sys_created_by=VMTrestuser"))
		.setHeader("CamelHttpMethod", constant("GET"))
		.setHeader("Cookie", constant("BIGipServerpool_sandbox=440453642.33598.0000; JSESSIONID=53ED26523B025D2B2CBC2139A53E3BDC; glide_user_route=glide.a1e7d977b734181b528e29363f053207; _ga=GA1.2.659009183.1412777426"))
		// TODO - Add error handler for unexpected response codes
		.filter(header("CamelHttpResponseCode").isEqualTo("200"))
		.unmarshal().json(JsonLibrary.Gson, Map.class) 
		
//		.choice()
//			.when().jsonpath("$.result[(@.approval='Approved')]")
//				.log(LoggingLevel.INFO, "Found approval")
//		.endChoice()
		.to("log:com.vmturbo.integration.camel?level=DEBUG&multiline=true&showAll=true")
		.end();
    }

}
